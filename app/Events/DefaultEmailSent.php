<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DefaultEmailSent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Email sent from
     * @var $from
     */
    public $from;

    /**
     * Email sent to
     * @var $to
     */
    public $to;

    /**
     * Email sent subject
     * @var $subject
     */
    public $subject;

    /**
     * Email sent message
     * @var $message
     */
    public $message;

    /**
     * Create a new event instance.
     *
     * @param $from
     * @param $to
     * @param $subject
     * @param $message
     */
    public function __construct($from, $to, $subject, $message)
    {
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->message = $message;
    }
}
