<?php

namespace App\Listeners;

use App\Email;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Email::create([
            'from' => $event->from,
            'to' => $event->to,
            'subject' => $event->subject,
            'message' => $event->message
        ]);
    }
}
