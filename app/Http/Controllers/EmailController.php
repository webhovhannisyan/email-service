<?php

namespace App\Http\Controllers;

use App\Events\DefaultEmailSent;
use App\Http\Requests\SendDefaultEmailRequest;
use App\Mail\DefaultEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    /**
     * Email sent from
     * @var $from
     */
    private $from;

    /**
     * Email sent to
     * @var $to
     */
    private $to;

    /**
     * Email sent subject
     * @var $subject
     */
    private $subject;

    /**
     * Email sent message
     * @var $message
     */
    private $message;

    /**
     * Init default params
     */
    public function __construct()
    {
        $this->from = env('MAIL_FROM_ADDRESS');
        $this->to = env('MAIL_TO_ADDRESS');
        $this->subject = 'Default email';
        $this->message = '';
    }

    /**
     * Send an email to destination and redirect back
     * @param SendDefaultEmailRequest $request
     * @return RedirectResponse
     */
    public function send(SendDefaultEmailRequest $request)
    {
        Mail::to($this->to)->send(
            (new DefaultEmail($request->message))->subject($this->subject)
        );

        event(new DefaultEmailSent(
            $this->from,
            $this->to,
            $this->subject,
            $request->message)
        );

        return redirect()
            ->back()
            ->with('message', 'Email sent successfully!');
    }
}
